/**
 * Created by Dmitry Vereykin on 7/24/2015.
 */
public class InvalidNameOfMonth extends Exception {
    public InvalidNameOfMonth(String name) {
        super("Error! " + name + " is invalid name for the month.");
    }
}
